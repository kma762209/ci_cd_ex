package kma762209.ci.cd.ex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiCdExApplication {
    public static void main(String[] args) {
        SpringApplication.run(CiCdExApplication.class, args);
    }
}
