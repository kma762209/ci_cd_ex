package kma762209.ci.cd.ex;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/main")
public class MainController {

    @GetMapping(value = "/object", produces = APPLICATION_JSON_VALUE)
    public Set<MainObject> getObjects() {
        return MainObjects.getObjects();
    }

    @GetMapping(value = "/object/{id}", produces = APPLICATION_JSON_VALUE)
    public MainObject getObject(@PathVariable Integer id) {
        return MainObjects.getObject(id);
    }
}
