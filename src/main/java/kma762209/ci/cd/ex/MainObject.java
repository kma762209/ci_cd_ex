package kma762209.ci.cd.ex;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MainObject {
    private Integer id;
    private String name;
    private Double value;
}
