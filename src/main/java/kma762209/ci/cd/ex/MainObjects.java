package kma762209.ci.cd.ex;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
public enum MainObjects {
    OBJECT1(1, "Object1", 100.00),
    OBJECT2(2, "Object2", 200.00),
    OBJECT3(3, "Object3", 300.00),
    OBJECT4(4, "Object4", 400.00),
    OBJECT5(5, "Object5", 500.00),
    OBJECT6(6, "Object6", 600.00),
    OBJECT7(7, "Object7", 700.00),
    OBJECT8(8, "Object8", 800.00),
    OBJECT9(9, "Object9", 900.00);

    private final Integer id;
    private final String name;
    private final Double value;

    private MainObject createObject() {
        return MainObject.builder()
                .id(id)
                .name(name)
                .value(value)
                .build();
    }

    public static Set<MainObject> getObjects() {
        return Arrays.stream(values()).map(MainObjects::createObject).collect(Collectors.toSet());
    }

    public static MainObject getObject(Integer id) {
        return getObjects().stream().filter(o -> o.getId().equals(id)).findAny().orElseThrow();
    }
}
