import kma762209.ci.cd.ex.CiCdExApplication;
import kma762209.ci.cd.ex.MainObject;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = CiCdExApplication.class)
@AutoConfigureMockMvc
public class MainTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void test1() {
        testById(1);
    }

    @Test
    public void test2() {
        testById(2);
    }

    @Test
    public void test3() {
        testById(3);
    }

    @SneakyThrows
    private void testById(Integer id) {
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/main/object/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(id))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Object" + id)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value", Matchers.is(Double.parseDouble(id + "00"))));
    }
}
